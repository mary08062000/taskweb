plugins {
    id("org.springframework.boot") version "2.5.6"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("plugin.spring") version "1.8.10"
    kotlin("jvm") version "1.8.10"
    application
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

// --------------------- openapi generator ---------------------
buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.openapitools:openapi-generator-gradle-plugin:6.4.0")
    }
}

tasks.register<Delete>("deleteRedundantGeneratedFiles") {
    delete(file("$buildDir/generated/.openapi-generator-ignore"))
    delete(file("$buildDir/generated/pom.xml"))
    delete(file("$buildDir/generated/README.md"))
}

tasks.register<Delete>("cleanServer") {
    delete("$buildDir/generated/src/main/kotlin/server")
}

tasks.register<org.openapitools.generator.gradle.plugin.tasks.GenerateTask>("buildServer").configure {
    dependsOn += tasks.getByName("cleanServer")

    // https://github.com/OpenAPITools/openapi-generator/blob/master/docs/generators/kotlin-spring.md
    // https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator/src/main/resources/kotlin-spring
    packageName.set("server")
    generatorName.set("kotlin-spring")
    inputSpec.set("${rootDir}/specs/openapi.yaml")
    outputDir.set("$buildDir/generated")
    library.set("spring-boot")
    configOptions.set(
        mapOf(
            "annotationLibrary" to "none",
            "documentationProvider" to "none",
            "enumPropertyNaming" to "original",
            "gradleBuildFile" to "false",
            "serializableModel" to "true",
            "serializationLibrary" to "jackson",
            "serverPort" to "8080",
            "serviceInterface" to "false",
            "interfaceOnly" to "true",
            "sortModelPropertiesByRequiredFlag" to "true",
            "sortParamsByRequiredFlag" to "true",
            "sourceFolder" to "src/main/kotlin",
            "useSwaggerUI" to "false",
            "useTags" to "true",
        )
    )

    finalizedBy(tasks.getByName("deleteRedundantGeneratedFiles"))
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    dependsOn += tasks.getByName("buildServer")
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}
java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

java.sourceSets["main"].java {
    srcDir("$buildDir/generated/src/main/kotlin")
}
// ------------------- openapi generator end -------------------
