package org.example.services

import org.springframework.stereotype.Service

@Service
class CharsService {

    fun usedChars(text: String): String {
        val set = HashSet<Char>()
        val result = StringBuilder()
        var firstChar = true
        for (char in text) {
            if (!char.isWhitespace() && !set.contains(char)) {
                set.add(char)
                if (firstChar) {
                    result.append(char)
                    firstChar = false
                } else {
                    result.append(", $char")
                }
            }
        }

        return result.toString()
    }
}
