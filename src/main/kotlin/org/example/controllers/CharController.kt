package org.example.controllers

import org.example.services.CharsService
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.Resource
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import server.apis.CharsApi

@Component
class CharController(
    val charsService: CharsService,
) : CharsApi {
    override fun usedCharsPost(body: Resource): ResponseEntity<String> {
        val text = String((body as ByteArrayResource).byteArray)
        val usedChars = charsService.usedChars(text)
        return ResponseEntity.ok(usedChars)
    }
}
