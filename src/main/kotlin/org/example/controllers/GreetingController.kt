package org.example.controllers

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import server.apis.GreetingApi

@Component
class GreetingController : GreetingApi {
    override fun rootGet(): ResponseEntity<String> {
        val greeting = "Добро пожаловать! \n" +
                "Чтобы совершить необходимое преобразование текста, выполните POST запрос по пути /used-chars, относительно текущего, с файлом в теле запроса. \n" +
                "Ссылка на проект : https://gitlab.com/mary08062000/taskweb"
        return ResponseEntity.ok(greeting)
    }
}
