#!/bin/bash

container_name=demo
image_name=${CI_REGISTRY_IMAGE}

echo "deploy container = $container_name from image = $image_name"

# grep old image id
old_image_id=$(docker image ls | grep "$image_name" | awk '{print $3}')
echo "old_image_id=$old_image_id"

# pull new image
echo "docker pull $image_name"
docker pull "$image_name"

# we need new image id to check if it's equal to old, and if not delete the old image
new_image_id=$(docker image ls | grep "$image_name" | awk '{print $3}')
echo "new_image_id=$new_image_id"

# stop old container and remove it with its volume
echo "stopping old container"
docker stop "$container_name"
docker rm -v "$container_name"

# start new container
echo "starting new container"
docker run --publish "8880:8080" \
  --restart unless-stopped \
  --name "$container_name" \
  -d "$image_name"

# delete the old image if it isn't equal to the new one
if [[ "$old_image_id" != "$new_image_id" ]]; then
  echo "remove old image $old_image_id"
  docker rmi "$old_image_id"
fi
